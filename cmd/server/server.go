package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	TELEGRAM_API_BASE   = "https://api.telegram.org/bot"
	TELEGRAM_API_METHOD = "/sendMessage?chat_id="
	TELEGRAM_API_TEXT   = "&text="
)

type telegramBot struct {
	authToken    string
	channelToken string
	channel      string
}

type msg struct {
	Text string `json:"text"`
}

func toTelegram(token string, chat string, payload string) {
	uri := TELEGRAM_API_BASE + token + TELEGRAM_API_METHOD + chat + TELEGRAM_API_TEXT + payload
	resp, err := http.Get(uri)
	if err != nil {
		log.Println("Error getting response. ", err)
	}
	defer resp.Body.Close()
}

// validMAC reports whether messageMAC is a valid HMAC tag for message.
func validMAC(message, messageMAC, key []byte) bool {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	expectedMAC := mac.Sum(nil)
	return hmac.Equal(messageMAC, expectedMAC)
}

func (t *telegramBot) boost(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var m msg
	err := decoder.Decode(&m)
	if err != nil {
		log.Println("error decoding")
		return
	}
	auth := r.Header.Get("Authorization")
	if auth == "" {
		log.Println("ERROR: cannot extract authorization token")
		return
	}
	tokenParts := strings.Split(auth, " ")
	if tokenParts[0] != "Bearer" {
		log.Println("ERROR: expected a bearer token")
		return
	}
	// log.Println(">>> token:", tokenParts[1])
	msgMAC, err := base64.StdEncoding.DecodeString(tokenParts[1])
	if err != nil {
		log.Println("ERROR: cannot decode authorization token")
		return
	}

	if !validMAC([]byte(m.Text), []byte(msgMAC), []byte(t.authToken)) {
		log.Println("ERROR: could not validate authorization token!")
		return
	}

	log.Println("Reboosting message:", m.Text)
	toTelegram(t.channelToken, t.channel, m.Text)
	fmt.Fprintf(w, "Auth OK. Message sent")
}

func main() {
	authToken := os.Getenv("REBOOSTER_TOKEN")
	channelToken := os.Getenv("TELEGRAM_TOKEN")
	channel := os.Getenv("TELEGRAM_CHANNEL")

	if authToken == "" || channelToken == "" || channel == "" {
		fmt.Println("Please set REBOOSTER_TOKEN, TELEGRAM_TOKEN and TELEGRAM_CHANNEL")
		os.Exit(1)
	}

	t := &telegramBot{authToken, channelToken, channel}

	http.HandleFunc("/reboost", t.boost)
	port := "8080"
	log.Println("OK, listening on port", port)
	http.ListenAndServe(":"+port, nil)
}
