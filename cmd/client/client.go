package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
)

const (
	uri   string = "http://localhost:8080/reboost"
	usage        = `Usage: rebooster "message"`
)

type msg struct {
	Text string `json:"text"`
}

func calculateMAC(message, key []byte) []byte {
	mac := hmac.New(sha256.New, key)
	mac.Write(message)
	theMAC := mac.Sum(nil)
	return theMAC
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println(usage)
		os.Exit(1)
	}
	msg := os.Args[1]
	if msg == "" {
		fmt.Println(usage)
		os.Exit(1)
	}
	var jsonStr = []byte(`{"text":"` + msg + `"}`)

	key := []byte(os.Getenv("REBOOSTER_TOKEN"))
	if string(key) == "" {
		log.Fatal("Please set REBOOSTER_TOKEN")
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", uri, bytes.NewBuffer(jsonStr))

	themac := calculateMAC([]byte(msg), key)
	token := base64.StdEncoding.EncodeToString([]byte(themac))
	bearerStr := "Bearer " + string(token)
	req.Header.Set("Authorization", bearerStr)

	log.Println("Sending msg:", msg)
	res, err := client.Do(req)
	if err != nil {
		log.Println("Error getting response. ", err)
	}
	defer res.Body.Close()
}
