import base64
import configparser
import hashlib
import hmac
import json
import os
import sys

from signald import Signal
import requests
import tinydb

# TODO 
# [ ] ensure messages are self-destroying

PREFIX = "PUBLICA"

config_vars = ('SIGNAL_NUMBER', 'REBOOSTER_TOKEN', 'GROUP', 'ENDPOINT')

defaults = {
        'ENDPOINT': "http://localhost:8080/reboost"
}

def getConfigFolder():
    path = os.path.expanduser('~/.config/rebooster')
    if not os.path.isdir(path):
        os.makedirs(path)
    return path

def getCacheFolder():
    path = os.path.expanduser('~/.cache/rebooster')
    if not os.path.isdir(path):
        os.makedirs(path)
    return path

class Config:

    def __init__(self):
        path = os.path.join(getConfigFolder(), 'config')
        self.c = configparser.ConfigParser()
        self.c.read(path)

    def readValue(self, var):
        val = self.readFromEnv(var)
        if not val:
            val = self.readFromFile(var)
        if not val:
            val = defaults.get(var)
        return val

    def readFromEnv(self, var):
        return os.environ.get(var)

    def readFromFile(self, var):
        return self.c['default'].get(var.lower())

c = Config()

def readConfig(var):
    return c.readValue(var)

def loadConfig():
    for var in config_vars:
        globals()[var] = readConfig(var)
        print(var, globals()[var])


def getHash(text):
    return hashlib.sha256(bytes(text, 'utf-8')).hexdigest()

class Database:

    def __init__(self, signalNumber):
        numberHash = getHash(signalNumber)
        path = os.path.join(getCacheFolder(), numberHash + '.db')
        self.db = tinydb.TinyDB(path)

    def haveSeen(self, text, timestamp):
        record = tinydb.Query()
        data = self.db.search((record.hash == getHash(text)) & (record.ts == timestamp))
        return bool(data)

    def markSeen(self, text, timestamp):
        self.db.insert({'hash': getHash(text), 'ts': timestamp})


def calculateMAC(message, key):
    return hmac.new(key, message, digestmod=hashlib.sha256).digest()

def reboostMessage(message):
    mac = calculateMAC(bytes(message, "utf-8"), bytes(REBOOSTER_TOKEN, "utf-8"))
    token = base64.b64encode(mac)
    bearerStr = b"Bearer " + token
    headers = {'Authorization': bearerStr}
    r = requests.post(ENDPOINT, json={'text': message}, headers=headers)

def main():
    loadConfig()
    if not SIGNAL_NUMBER or not REBOOSTER_TOKEN or not GROUP:
        print("Please set SIGNAL_NUMBER, REBOOSTER_TOKEN and GROUP")
        sys.exit(1)

    db = Database(SIGNAL_NUMBER)
    s = Signal(SIGNAL_NUMBER)


    print("> receiving messages...")
    for message in s.receive_messages():
        if message.group_info:
            if message.group_info.get('name') == GROUP and message.text.startswith(PREFIX):
                text = message.text.strip(PREFIX).strip()
                if not db.haveSeen(text, message.timestamp):
                    reboostMessage(text)
                    db.markSeen(text, message.timestamp)
                    print("> Reboosted! [", message.timestamp, "] ", text)

if __name__ == "__main__":
    main()
